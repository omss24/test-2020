from rest_framework.test import APITestCase
from rest_framework import status as status_definitions
from rest_framework_simplejwt.tokens import RefreshToken

from django.contrib.auth.models import User

from faker import Faker


class WorkyTestBase(APITestCase):
    def setUp(self):
        self.fake = Faker()

        user_data = self._get_new_user_data()
        self.user_password = user_data['password']

        self.user = User(**user_data)
        self.user.set_password(self.user_password)
        self.user.save()

        self.refresh = RefreshToken.for_user(self.user)

    def tearDown(self):
        User.objects.get(pk=self.user.pk).delete()

    def jwt_login(self):
        self.client.credentials(
            HTTP_AUTHORIZATION='Bearer {}'.format(self.refresh.access_token)
        )

    def jwt_logout(self):
        self.client.credentials()

    def _test_response(self, response, status=status_definitions.HTTP_200_OK):
        self.assertEqual(
            response.status_code,
            status,
            msg=response.content
        )

    def _get_new_user_data(self):
        email = self.fake.email()
        return {
            'email': email,
            'first_name': self.fake.name(),
            'last_name': self.fake.name(),
            'password': self.fake.password(),
            'username': email,
            'is_staff': True,
            'is_active': True
        }


class TestJWTAuth(WorkyTestBase):
    def test_verify_jwt(self):
        response = self.client.post(
            '/token/',
            {
                'username': self.user.username,
                'password': self.user_password
            }
        )

        self._test_response(response)
        self.assertNotEqual(response.data['access'], None)
        self.assertNotEqual(response.data['refresh'], None)


class UserListRetriveTest(WorkyTestBase):
    url = '/users/'

    def test_password_representation(self):
        self.jwt_login()

        response = self.client.get(self.url + str(self.user.pk) + '/')

        self._test_response(response)
        self.assertEqual(response.data['password'], '--')

        response = self.client.get(self.url)

        self._test_response(response)

        for user_in_response in response.data:
            self.assertEqual(user_in_response['password'], '--')

    def test_get_list(self):
        self.jwt_logout()

        response = self.client.get(self.url)

        self._test_response(response)

        self.assertEqual(
            len(response.data),
            User.objects.all().count()
        )

    def test_get_one_user(self):
        self.jwt_logout()

        response = self.client.get(self.url + str(self.user.pk) + '/')

        self._test_response(response)

        self.assertEqual(response.data['pk'], self.user.pk)
        self.assertEqual(response.data['username'], self.user.username)

    def test_create_user_url(self):
        self.jwt_login()

        response = self.client.post(
            '/create_user/',
            self._get_new_user_data()
        )

        self._test_response(
            response,
            status=status_definitions.HTTP_201_CREATED
        )

        user_response = response.data
        del user_response['password']

        self.assertTrue(
            User.objects.filter(**user_response).exists(),
            msg='User not found: {}'.format(user_response)
        )

    def test_get_service_invalid_user(self):
        response = self.client.get(self.url + '2376/')

        self._test_response(
            response,
            status=status_definitions.HTTP_404_NOT_FOUND
        )

    def test_post_no_auth(self):
        self.jwt_logout()

        response = self.client.post(self.url)
        self._test_response(
            response,
            status=status_definitions.HTTP_401_UNAUTHORIZED
        )
