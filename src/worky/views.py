from django.contrib.auth.models import User
from worky.serializers import UserSerializer
from rest_framework import mixins, viewsets


class UserListRetriveCreate(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
