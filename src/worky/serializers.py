from django.contrib.auth.models import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'pk',
            'username',
            'email',
            'first_name',
            'last_name',
            'password'
        ]

    def to_representation(self, instance):
        represented = super(UserSerializer, self).to_representation(instance)

        # Clean password field for security.
        if 'password' in represented:
            represented['password'] = '--'

        return represented
